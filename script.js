let trainer={
	name: "Ash Kechum",
	age: 10,
	friends: {
		hoen: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	pokemon: ["Pikachu" , "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}
console.log(trainer);

console.log("Result of dot notation");
console.log(trainer.name);

console.log("Result of square bracket notation");
console.log(trainer ['pokemon']);

console.log("Result of talk method");
trainer.talk();

// ____________________________________________________
console.log(">>>>SECOND PART<<<<<<");
// ____________________________________________________

function Pokemon(name,level){
	// Properties
	this.name=name;
	this.level=level;
	this.health= 2*level;
	this.attack= level;

	// Methods
	this.tackle=function(target){
		let targetHealth =target.health - this.attack;
		let myPokemontHealth =this.health - this.attack;
		console.log( this.name + " tackled " + target.name);

		if (targetHealth <= 0 && myPokemontHealth>= 1) {
		console.log(target.name +"'s health is now reduced to " + targetHealth );
		console.log( target.name + " has fainted ");
		}
	

		if (targetHealth >= 1 && myPokemontHealth<= 0) {
		console.log(target.name +" won. ");
		console.log(this.name +"'s health is now reduced to " + myPokemontHealth );
		console.log( target.name + " is still alive.");
		}

		if (targetHealth >= 1 && myPokemontHealth>= 1) {
		console.log(target.name +"'s health is now reduced to " + myPokemontHealth );
		console.log( target.name + " is still alive.");
		}

		if ( targetHealth<= 0 && myPokemontHealth <= 0){
		console.log(" It's a draw.");
		}
	
	
	};
}

let pikachu= new Pokemon ("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);
let geodude= new Pokemon ("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);
rattata.tackle(geodude);